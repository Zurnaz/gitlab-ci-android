# Gitlab CI Android Sample Project

This is the sample project used in the Gitlab blog post [Setting Up Gitlab CI for Android Projects](https://about.gitlab.com/2016/11/30/setting-up-gitlab-ci-for-android-projects/).


[![pipeline status](https://gitlab.com/Zurnaz/gitlab-ci-android/badges/master/pipeline.svg)](https://gitlab.com/Zurnaz/gitlab-ci-android/commits/master)


[![coverage report](https://gitlab.com/Zurnaz/gitlab-ci-android/badges/master/coverage.svg)](https://gitlab.com/Zurnaz/gitlab-ci-android/commits/master)